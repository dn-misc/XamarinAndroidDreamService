﻿
using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Service.Dreams;
using Android.Widget;

namespace DayDreamTest
{
    public class MyDreamService : DreamService
    {
     
        public override void OnAttachedToWindow()
        {
            base.OnAttachedToWindow();

            //Allow user touch
            this.Interactive = true;

            //Set full screen
            this.Fullscreen = true;

            //set layout
            //var layout = new LinearLayout(this);
            //layout.Orientation = Orientation.Vertical;

            //var aLabel = new TextView(this);
            //aLabel.Text = "Hello, World!!!";

            //var bLabel = new TextView(this);
            //bLabel.Text = "More Text";

            //layout.AddView(aLabel);
            //layout.AddView(bLabel);

            //SetContentView (layout); 

            // Set the dream layout
            TextView txtView = new TextView(this);
            txtView.Text = "Hello DayDream !!";

            SetContentView(txtView);


        }

    }


}
